# ColorSelector

The ColorSelector is a QControl and requires the QControl Toolkit, http://bit.ly/QControlNITools.  It inherits from and extends the Picture control.  It implements the RGB spectrum and allows the user to select a color.